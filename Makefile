all:
	gcc -Wall atmelice.c main.c -lusb -o avrusersig

test:
	./avrusersig sigdata.bin

avrdude:
	avrdude -c atmelice -p m256rfr2

clean:
	rm -rf avrusersig


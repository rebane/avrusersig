#include "atmelice.h"
#include <stdio.h>
#include <string.h>
#include <usb.h>

#define ATMELICE_VID    0x03eb
#define ATMELICE_PID    0x2141
#define ATMELICE_EP_OUT 0x01
#define ATMELICE_EP_IN  0x82

// #define ATMELICE_DEBUG_RAW
// #define ATMELICE_DEBUG_CMD

void atmelice_init(){
	usb_init();         // initialize the library
	usb_find_busses();  // find all busses
	usb_find_devices(); // find all connected devices
}

int atmelice_open(atmelice_t *atmelice){
	struct usb_bus *bus;
	struct usb_device *dev;

	for(bus = usb_get_busses(); bus; bus = bus->next){
		for(dev = bus->devices; dev; dev = dev->next){
			if((dev->descriptor.idVendor == ATMELICE_VID) && (dev->descriptor.idProduct == ATMELICE_PID)){
				atmelice->handler = usb_open(dev);
				if(usb_claim_interface(atmelice->handler, 0) < 0){
					atmelice->handler = NULL;
					return(-1);
				}
				atmelice->serial = 0;
				return(1);
			}
            	}
        }
	atmelice->handler = NULL;
	return(-1);
}

int atmelice_transfer_raw(atmelice_t *atmelice, const void *out, int outlen, void *in, int inlen){
	char buffer[512];
#ifdef ATMELICE_DEBUG_RAW
	int i;
#endif
	int l;
	if(outlen > 512)outlen = 512;

#ifdef ATMELICE_DEBUG_RAW
	printf("interrupt_write: % 2d,", outlen);
	for(i = 0; i < 48; i++){
		printf(" %02X", (unsigned int)((unsigned char *)out)[i]);
	}
	printf("\n");
#endif

	memset(buffer, 0, 512);
	memcpy(buffer, out, outlen);
	l = usb_interrupt_write(atmelice->handler, ATMELICE_EP_OUT, buffer, 512, 2000);
	if(l < 0)return(l);
	l = usb_interrupt_read(atmelice->handler, ATMELICE_EP_IN, buffer, 512, 5000);
	if(l < 0)return(l);

#ifdef ATMELICE_DEBUG_RAW
	printf("interrupt_read: %d,", l);
	for(i = 0; i < 48; i++){
		printf(" %02X", (unsigned int)((unsigned char *)buffer)[i]);
	}
	printf("\n");
#endif

	if(l > inlen)l = inlen;
	memcpy(in, buffer, inlen);
	return(l);
}

int atmelice_transfer_cmd(atmelice_t *atmelice, const void *out, int outlen, void *in, int inlen){
	uint8_t buffer[512];
	int i, l, l2;

#ifdef ATMELICE_DEBUG_CMD
	printf("ATMELICE: CMD: %d,", outlen);
	for(i = 0; i < outlen; i++){
		printf(" %02X", (unsigned int)((unsigned char *)out)[i]);
	}
	printf("\n");
#endif

	if(outlen > (512 - 8))outlen = (512 - 8);
	buffer[0] = 0x80;
	buffer[1] = 0x11;
	buffer[2] = ((outlen + 4) >> 8) & 0xFF;
	buffer[3] = ((outlen + 4) >> 0) & 0xFF;
	buffer[4] = 0x0E;
	buffer[5] = 0x00;
	buffer[6] = (atmelice->serial >> 0) & 0xFF;
	buffer[7] = (atmelice->serial >> 8) & 0xFF;
	memcpy(&buffer[8], out, outlen);
	l = atmelice_transfer_raw(atmelice, buffer, outlen + 8, buffer, 512);
	if(l < 0)return(l);
	if(l < 2)return(-1);
	if((buffer[0] != 0x80) || (buffer[1] != 0x01))return(-1);

	for(i = 0; i < 10; i++){
		l = atmelice_transfer_raw(atmelice, "\x81", 1, buffer, 512);
		if(l < 0)return(l);
		if(l < 4)continue;
		if((buffer[0] != 0x81) || (buffer[1] != 0x11))continue;
		l2 = (((uint16_t)buffer[2] << 8) | buffer[3]);
		if(l2 < 3)continue;
		if(l < (l2 + 4))continue;
		if(buffer[4] != 0x0E)continue;
		if(atmelice->serial != (((uint16_t)buffer[5] << 0) | buffer[6]))continue;
		l2 -= 3;
		if(l2 > inlen)l2 = inlen;
		memcpy(in, &buffer[7], l2);

#ifdef ATMELICE_DEBUG_CMD
		printf("ATMELICE: RSP: %d,", l2);
		for(i = 0; i < l2; i++){
			printf(" %02X", (unsigned int)((unsigned char *)in)[i]);
		}
		printf("\n");
#endif

		atmelice->serial++;
		return(l2);
	}
	return(-1);
}

void atmelice_close(atmelice_t *atmelice){
	usb_close(atmelice->handler);
}


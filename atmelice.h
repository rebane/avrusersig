#ifndef _ATMELICE_H_
#define _ATMELICE_H_

#include <stdint.h>
#include <usb.h>

struct atmelice_struct{
	usb_dev_handle *handler;
	uint16_t serial;
};

typedef struct atmelice_struct atmelice_t;

void atmelice_init();
int atmelice_open(atmelice_t *atmelice);
int atmelice_transfer_raw(atmelice_t *atmelice, const void *out, int outlen, void *in, int inlen);
int atmelice_transfer_cmd(atmelice_t *atmelice, const void *out, int outlen, void *in, int inlen);
void atmelice_close(atmelice_t *atmelice);

#endif


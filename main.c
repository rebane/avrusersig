#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "atmelice.h"

uint8_t userpages[768];

int main(int argc, char *argv[]){
	atmelice_t atmelice;
	char buffer[512];
	int l, fd;

	printf("\nAVR user signature writer by Rebane, 2017 (rebane@alkohol.ee)\n\n");

	if(argc < 2){
		fprintf(stderr, "missing file name\n");
		return(1);
	}

	memset(userpages, 0xFF, 768);
	fd = open(argv[1], O_RDONLY);
	if(fd < 0){
		fprintf(stderr, "cannot open file\n");
		return(1);
	}
	if(read(fd, userpages, 768) < 0){
		fprintf(stderr, "file read error\n");
		close(fd);
		return(1);
	}
	close(fd);

	atmelice_init();
	if(atmelice_open(&atmelice) < 0){
		fprintf(stderr, "error opening atmelice\n");
		return(1);
	}

	printf("Preparing 1...\n");
	l = atmelice_transfer_raw(&atmelice, "\x02\x01", 2, buffer, 512);
	if(l < 0){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Preparing 2...\n");
	l = atmelice_transfer_raw(&atmelice, "\x01\x00\x01", 3, buffer, 512);
	if(l < 0){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Sign in...\n");
	l = atmelice_transfer_cmd(&atmelice, "\x01\x10\x00", 3, buffer, 512);
	if((l != 3) || memcmp(buffer, "\x01\x80\x00", 3)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Set parameter (scope 0x12, section 0, param 0)...\n");
	l = atmelice_transfer_cmd(&atmelice, "\x12\x01\x00\x00\x00\x01\x02", 7, buffer, 512);
	if((l != 3) || memcmp(buffer, "\x12\x80\x00", 3)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Set parameter (scope 0x12, section 0, param 1)...\n");
	l = atmelice_transfer_cmd(&atmelice, "\x12\x01\x00\x00\x01\x01\x01", 7, buffer, 512);
	if((l != 3) || memcmp(buffer, "\x12\x80\x00", 3)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Set parameter (scope 0x12, section 1, param 0)...\n");
	l = atmelice_transfer_cmd(&atmelice, "\x12\x01\x00\x01\x00\x01\x04", 7, buffer, 512);
	if((l != 3) || memcmp(buffer, "\x12\x80\x00", 3)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Set parameter (scope 0x12, section 1, param 1)...\n");
	l = atmelice_transfer_cmd(&atmelice, "\x12\x01\x00\x01\x01\x04\x00\x00\x00\x00", 10, buffer, 512);
	if((l != 3) || memcmp(buffer, "\x12\x80\x00", 3)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Set parameter (scope 0x12, section 2, param 0)...\n");
	l = atmelice_transfer_cmd(&atmelice, "\x12\x01\x00\x02\x00\x1f\x00\x01\x00\x00\x04\x00\x00\x00\x00\x00\x00\xfe\x01\x00\x00\x00\x00\x20\x08\x04\x01\x00\x00\x00\x31\x22\x21\x1f\x20\x57\x00", 37, buffer, 512);
	if((l != 3) || memcmp(buffer, "\x12\x80\x00", 3)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Sign on...\n");
	l = atmelice_transfer_cmd(&atmelice, "\x12\x10\x00\x01", 4, buffer, 512);
	if((l < 2) || memcmp(buffer, "\x12\x84", 2)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Enter progmode...\n");
	l = atmelice_transfer_cmd(&atmelice, "\x12\x15\x00", 3, buffer, 512);
	if((l != 3) || memcmp(buffer, "\x12\x80\x00", 3)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Read signature...\n");
	l = atmelice_transfer_cmd(&atmelice, "\x12\x21\x00\xb4\x00\x00\x00\x00\x03\x00\x00\x00", 12, buffer, 512);
	if((l != 7) || memcmp(buffer, "\x12\x84\x00\x1E\xA8\x02\x00", 7)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Erase user page 0x100...\n");
	l = atmelice_transfer_cmd(&atmelice, "\x12\x20\x00\x07\x00\x01\x00\x00", 8, buffer, 512);
	if((l != 3) || memcmp(buffer, "\x12\x80\x00", 3)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Erase user page 0x200...\n");
	l = atmelice_transfer_cmd(&atmelice, "\x12\x20\x00\x07\x00\x02\x00\x00", 8, buffer, 512);
	if((l != 3) || memcmp(buffer, "\x12\x80\x00", 3)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Erase user page 0x300...\n");
	l = atmelice_transfer_cmd(&atmelice, "\x12\x20\x00\x07\x00\x03\x00\x00", 8, buffer, 512);
	if((l != 3) || memcmp(buffer, "\x12\x80\x00", 3)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Write user page 0x100...\n");
	memcpy(&buffer[0], "\x12\x23\x00\xc5\x00\x01\x00\x00\x00\x01\x00\x00\x00", 13);
	memcpy(&buffer[13], &userpages[0], 256);
	l = atmelice_transfer_cmd(&atmelice, buffer, 13 + 256, buffer, 512);
	if((l != 3) || memcmp(buffer, "\x12\x80\x00", 3)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Write user page 0x200...\n");
	memcpy(buffer, "\x12\x23\x00\xc5\x00\x02\x00\x00\x00\x01\x00\x00\x00", 13);
	memcpy(&buffer[13], &userpages[256], 256);
	l = atmelice_transfer_cmd(&atmelice, buffer, 13 + 256, buffer, 512);
	if((l != 3) || memcmp(buffer, "\x12\x80\x00", 3)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Write user page 0x300...\n");
	memcpy(buffer, "\x12\x23\x00\xc5\x00\x03\x00\x00\x00\x01\x00\x00\x00", 13);
	memcpy(&buffer[13], &userpages[512], 256);
	l = atmelice_transfer_cmd(&atmelice, buffer, 13 + 256, buffer, 512);
	if((l != 3) || memcmp(buffer, "\x12\x80\x00", 3)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Verify user page 0x100...\n");
	l = atmelice_transfer_cmd(&atmelice, "\x12\x21\x00\xc5\x00\x01\x00\x00\x00\x01\x00\x00\x00", 13, buffer, 512);
	if((l != (3 + 256 + 1)) || memcmp(buffer, "\x12\x84\x00", 3) || (buffer[259] != 0x00) || memcmp(&userpages[0], &buffer[3], 256)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Verify user page 0x200...\n");
	l = atmelice_transfer_cmd(&atmelice, "\x12\x21\x00\xc5\x00\x02\x00\x00\x00\x01\x00\x00\x00", 13, buffer, 512);
	if((l != (3 + 256 + 1)) || memcmp(buffer, "\x12\x84\x00", 3) || (buffer[259] != 0x00) || memcmp(&userpages[256], &buffer[3], 256)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	printf("Verify user page 0x300...\n");
	l = atmelice_transfer_cmd(&atmelice, "\x12\x21\x00\xc5\x00\x03\x00\x00\x00\x01\x00\x00\x00", 13, buffer, 512);
	if((l != (3 + 256 + 1)) || memcmp(buffer, "\x12\x84\x00", 3) || (buffer[259] != 0x00) || memcmp(&userpages[512], &buffer[3], 256)){
		fprintf(stderr, "transfer error\n");
		goto error;
	}

	atmelice_close(&atmelice);
	return(0);
error:
	atmelice_close(&atmelice);
	return(1);
}

